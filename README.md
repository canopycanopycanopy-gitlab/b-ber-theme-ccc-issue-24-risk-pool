# `b-ber-theme-ccc-issue-24-risk-pool`

A Triple Canopy specific b-ber theme that extends [b-ber-theme-ccc](https://gitlab.com/canopycanopycanopy/b-ber-themes/b-ber-theme-ccc)

## Install

```
$ cd my-project
$ npm init -y
$ npm install --save git+https://gitlab.com/canopycanopycanopy/b-ber-themes/b-ber-theme-ccc-issue-24-risk-pool.git
$ bber theme set b-ber-theme-ccc-issue-24-risk-pool
```

## Update

In order to pull down new commits to the repo, follow these instructions in individual b-ber projects.

```
$ npm update b-ber-theme-ccc-issue-24-risk-pool
```

## Issues

Follow the steps [here](https://gitlab.com/canopycanopycanopy/b-ber-themes/b-ber-theme-ccc/blob/master/README.md#issues) if encountering issues installing this theme.
